package user_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_user"
	"final-project-bds-sanbercode-golang-batch-31/service/user_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type UserControllerImpl struct {
	UserService user_service.UserService
}

func NewUserController(userService user_service.UserService) UserController {
	return &UserControllerImpl{UserService: userService}
}

// Login godoc
// @Summary User Login.
// @Description Digunakan untuk login ke akun user.
// @Tags Area User
// @Param Body body web_user.UserLoginRequest true "the body to login"
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_user.UserLoginResponse}
// @Router /login [post]
func (u *UserControllerImpl) Login(c *gin.Context) {
	loginReq := web_user.UserLoginRequest{}

	err := c.ShouldBindJSON(&loginReq)
	helper.PanicIfError(err)

	loginResp := u.UserService.Login(c, loginReq)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   loginResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Register godoc
// @Summary User Register.
// @Description Digunakan oleh user untuk mendaftarkan akun baru.
// @Tags Area User
// @Param Body body web_user.UserRegisterRequest true "the body to register (phone example: +62081232323)"
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_user.UserResponse}
// @Router /register [post]
func (u *UserControllerImpl) Register(c *gin.Context) {
	registerReq := web_user.UserRegisterRequest{}

	err := c.ShouldBindJSON(&registerReq)
	helper.PanicIfError(err)

	registerResp := u.UserService.Register(c, registerReq)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   registerResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Verif Akun godoc
// @Summary User Verif Akun.
// @Description Digunakan oleh user untuk verifikasi akun (link verifikasi dikirimkan melalui SMS).
// @Tags Area User
// @Param token path string true "token for verification"
// @Param id path string true "id for verification"
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /verification/{id}/{token} [get]
func (u *UserControllerImpl) Verif(c *gin.Context) {

	userId := c.Param("id")
	tokenVerif := c.Param("token")

	if userId == "" {
		panic(exception.NewCustomValidationError("Id user tidak boleh kosong"))
	}

	if tokenVerif == "" {
		panic(exception.NewCustomValidationError("Token verifikasi tidak boleh kosong"))
	}

	id, err := strconv.Atoi(userId)
	helper.PanicIfError(err)

	u.UserService.VerifUser(c, id, tokenVerif)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Verifikasi Akun Sukses",
	}

	c.JSON(http.StatusOK, webResponse)
}

// UpdatePassword godoc
// @Summary User Merubah Password.
// @Description Digunakan oleh user untuk nerubah password akunnya.
// @Tags Area User
// @Param Body body web_user.UserUpdatePasswordRequest true "the body to update password"
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_user.UserResponse}
// @Router /reset-password [post]
func (u *UserControllerImpl) UpdatePassword(c *gin.Context) {
	updateReq := web_user.UserUpdatePasswordRequest{}

	err := c.ShouldBindJSON(&updateReq)
	helper.PanicIfError(err)

	updateResp := u.UserService.UpdatePassword(c, updateReq)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   updateResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Resend Verification Link godoc
// @Summary User Resend Verification Link.
// @Description Digunakan oleh user ketika ingin mengirimkan link verifikiasi ulang melalui sms.
// @Tags Area User
// @Param username path string true "username"
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /resend-verification/{username} [get]
func (u *UserControllerImpl) ResendToken(c *gin.Context) {
	username := c.Param("username")

	if username == "" {
		panic(exception.NewCustomValidationError("Username tidak boleh kosong"))
	}

	u.UserService.ResendSMS(c, username)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Silahkan cek kotak pesan anda",
	}

	c.JSON(http.StatusOK, webResponse)
}
