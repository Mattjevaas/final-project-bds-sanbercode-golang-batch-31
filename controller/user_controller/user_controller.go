package user_controller

import "github.com/gin-gonic/gin"

type UserController interface {
	Login(c *gin.Context)
	Register(c *gin.Context)
	Verif(c *gin.Context)
	UpdatePassword(c *gin.Context)
	ResendToken(c *gin.Context)
}