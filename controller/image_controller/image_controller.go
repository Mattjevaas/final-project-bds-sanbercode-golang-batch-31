package image_controller

import "github.com/gin-gonic/gin"

type ImageController interface {
	Add(c *gin.Context)
	Delete(c *gin.Context)
}
