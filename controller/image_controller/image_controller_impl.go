package image_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"final-project-bds-sanbercode-golang-batch-31/service/image_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type ImageControllerImpl struct {
	ImageService image_service.ImageService
}

func NewImageController(imageService image_service.ImageService) ImageController {
	return &ImageControllerImpl{ImageService: imageService}
}

// Add Image Thread godoc
// @Summary User Add Thread Image.
// @Description Digunakan oleh user untuk menambahkan image url pada thread.
// @Tags Image
// @Param Body body web_image.ImageRequest true "the body to updating thread"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /image [post]
func (i *ImageControllerImpl) Add(c *gin.Context) {
	imageReq := web_image.ImageRequest{}

	err := c.BindJSON(&imageReq)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	imageReq.IdUserFk = user

	i.ImageService.Add(c, imageReq)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil menambahkan image url",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Delete Image Thread godoc
// @Summary User Delete Thread Image.
// @Description Digunakan oleh user untuk menghapus image url pada thread.
// @Tags Image
// @Param imageId path string true "image id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /image/{imageId} [delete]
func (i *ImageControllerImpl) Delete(c *gin.Context) {

	paramId := c.Param("imageId")
	if paramId == "" {
		panic(exception.NewCustomValidationError("Id image tidak boleh kosong"))
	}

	imageId , err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	i.ImageService.Delete(c, user, imageId)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil menghapus image url",
	}

	c.JSON(http.StatusOK, webResponse)
}


