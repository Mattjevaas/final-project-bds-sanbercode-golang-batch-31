package rating_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_rating"
	"final-project-bds-sanbercode-golang-batch-31/service/rating_service"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RatingControllerImpl struct {
	RatingService rating_service.RatingService
}

func NewRatingController(ratingService rating_service.RatingService) RatingController {
	return &RatingControllerImpl{RatingService: ratingService}
}

// Rating a Thread godoc
// @Summary User Rating a Thread.
// @Description Digunakan oleh user untuk memberikan rating pada sebuah Thread.
// @Tags Rating
// @Param Body body web_rating.RatingCreateRequest true "the body to rating thread"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /rating [post]
func (r *RatingControllerImpl) Rate(c *gin.Context) {
	ratingReq := web_rating.RatingCreateRequest{}

	err := c.ShouldBindJSON(&ratingReq)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	ratingReq.IdUserFk = user

	r.RatingService.Rate(c, ratingReq)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil memberikan thread rating",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Change Thread Rate godoc
// @Summary User Change Rate
// @Description Digunakan oleh user untuk mengganti rating sebuah thread yang pernah ia rate
// @Tags Rating
// @Param Body body web_rating.RatingCreateRequest true "the body to rating thread"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /rating [put]
func (r *RatingControllerImpl) ChangeRate(c *gin.Context) {
	ratingReq := web_rating.RatingCreateRequest{}

	err := c.ShouldBindJSON(&ratingReq)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	ratingReq.IdUserFk = user

	 r.RatingService.ChangeRate(c,ratingReq)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil ubah rating thread",
	}

	c.JSON(http.StatusOK, webResponse)
}

