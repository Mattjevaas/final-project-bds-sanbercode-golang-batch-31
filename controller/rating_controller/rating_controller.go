package rating_controller

import "github.com/gin-gonic/gin"

type RatingController interface {
	Rate(c *gin.Context)
	ChangeRate(c *gin.Context)
}
