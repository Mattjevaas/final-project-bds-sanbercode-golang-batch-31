package comment_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"final-project-bds-sanbercode-golang-batch-31/service/comment_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type CommentControllerImpl struct {
	CommentService comment_service.CommentService
}

func NewCommentController(commentService comment_service.CommentService) CommentController {
	return &CommentControllerImpl{CommentService: commentService}
}

// Create Comment godoc
// @Summary User Comment on Thread.
// @Description Digunakan oleh user untuk melakukan comment pada sebuah thread.
// @Tags Comment
// @Param Body body web_comment.CommentCreateRequest true "the body to comment thread"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /comment [post]
func (co *CommentControllerImpl) AddComment(c *gin.Context) {
	commentReq := web_comment.CommentCreateRequest{}

	err := c.BindJSON(&commentReq)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	commentReq.IdUserFk = user

	co.CommentService.CreateComment(c, commentReq)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil menambahkan komentar",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Delete Comment godoc
// @Summary User Deleting Comment on Thread.
// @Description Digunakan oleh user untuk menghapus comment pada sebuah thread.
// @Tags Comment
// @Param commentId path string true "comment id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /comment/{commentId} [delete]
func (co *CommentControllerImpl) Delete(c *gin.Context) {
	paramId := c.Param("commentId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Comment ID tidak boleh kosong"))
	}

	commentId , err := strconv.Atoi(paramId)
	helper.PanicIfError(err)


	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	co.CommentService.Delete(c, user, commentId)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil menghapus komentar",
	}

	c.JSON(http.StatusOK, webResponse)
}
