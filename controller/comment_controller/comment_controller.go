package comment_controller

import "github.com/gin-gonic/gin"

type CommentController interface {
	AddComment(c *gin.Context)
	Delete(c *gin.Context)
}
