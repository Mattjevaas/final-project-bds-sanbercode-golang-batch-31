package like_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/service/like_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type LikeControllerImpl struct {
	LikeService like_service.LikeService
}

func NewLikeController(likeService like_service.LikeService) LikeController {
	return &LikeControllerImpl{LikeService: likeService}
}

// Like on comment godoc
// @Summary User Like on Comment.
// @Description Digunakan oleh user untuk melakukan like pada sebuah comment
// @Tags Like
// @Param commentId path string true "comment id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /like/{commentId} [post]
func (l LikeControllerImpl) Like(c *gin.Context) {

	commentId := c.Param("commentId")

	if commentId == ""{
		panic(exception.NewCustomValidationError("Id comment tidak boleh kosong"))
	}

	id, err := strconv.Atoi(commentId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	l.LikeService.Like(c, id, user)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil like comment",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Undo Like on comment godoc
// @Summary User Undo Like on Comment.
// @Description Digunakan oleh user untuk membatalkan like pada sebuah comment.
// @Tags Like
// @Param commentId path string true "comment id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /like/{commentId} [delete]
func (l LikeControllerImpl) Unlike(c *gin.Context) {
	commentId := c.Param("commentId")

	if commentId == ""{
		panic(exception.NewCustomValidationError("Id comment tidak boleh kosong"))
	}

	id, err := strconv.Atoi(commentId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	l.LikeService.Unlike(c, id, user)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil unlike comment",
	}

	c.JSON(http.StatusOK, webResponse)
}


