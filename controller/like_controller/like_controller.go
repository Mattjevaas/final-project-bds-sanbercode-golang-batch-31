package like_controller

import "github.com/gin-gonic/gin"

type LikeController interface {
	Like(c *gin.Context)
	Unlike(c *gin.Context)
}