package saved_thread_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/service/saved_thread_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type SavedThreadControllerImpl struct {
	SavedThreadService saved_thread_service.SavedThreadService
}


func NewSavedThreadController(
	savedThreadService saved_thread_service.SavedThreadService,
) SavedThreadController {
	return &SavedThreadControllerImpl{
		SavedThreadService: savedThreadService,
	}
}

// Save Someone Thread godoc
// @Summary User Saving Someone Thread.
// @Description Digunakan oleh user untuk menyimpan thread user lain.
// @Tags Saved Thread
// @Param threadId path string true "thread id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /saved-thread/save/{threadId} [post]
func (s *SavedThreadControllerImpl) SaveThread(c *gin.Context) {

	paramId := c.Param("threadId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Thread ID tidak boleh kosong"))
	}

	idThread, err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	s.SavedThreadService.SaveThread(c, user, idThread)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil simpan thread",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Delete Saved Thread godoc
// @Summary User Delete Saved Thread.
// @Description Digunakan user untuk menghapus thread yang ia simpan.
// @Tags Saved Thread
// @Param threadId path string true "thread id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /saved-thread/delete/{threadId} [delete]
func (s *SavedThreadControllerImpl) DeleteSavedThread(c *gin.Context) {

	paramId := c.Param("threadId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Thread ID tidak boleh kosong"))
	}

	idThread, err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	s.SavedThreadService.DeleteSavedThread(c, user, idThread)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil hapus thread tersimpan",
	}

	c.JSON(http.StatusOK, webResponse)
}

// Get Saved Thread godoc
// @Summary User Get All Saved Thread.
// @Description Digunakan oleh user untuk mendapatkan daftar thread yang disimpan.
// @Tags Saved Thread
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=[]web_saved_thread.SavedThreadResponse}
// @Router /saved-thread [get]
func (s *SavedThreadControllerImpl) FindAllSavedThread(c *gin.Context) {
	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	savedResps := s.SavedThreadService.FindAllSavedThread(c, user)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   savedResps,
	}

	c.JSON(http.StatusOK, webResponse)
}


