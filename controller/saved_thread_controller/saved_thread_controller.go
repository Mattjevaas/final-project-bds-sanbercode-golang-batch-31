package saved_thread_controller

import (
	"github.com/gin-gonic/gin"
)

type SavedThreadController interface {
	SaveThread(c *gin.Context)
	DeleteSavedThread(c *gin.Context)
	FindAllSavedThread(c *gin.Context)
}
