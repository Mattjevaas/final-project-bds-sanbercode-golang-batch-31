package thread_controller

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_thread"
	"final-project-bds-sanbercode-golang-batch-31/service/thread_service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type ThreadControllerImpl struct {
	ThreadService thread_service.ThreadService
}

func NewThreadController(
	threadService thread_service.ThreadService,
) ThreadController {
	return &ThreadControllerImpl{
		ThreadService: threadService,
	}
}

// Create Thread godoc
// @Summary User Create New Thread.
// @Description Digunakan oleh user untuk membuat Thread / Posting baru.
// @Tags Thread
// @Param Body body web_thread.ThreadCreateRequest true "the body to creating thread"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_thread.ThreadResponse}
// @Router /thread [post]
func (t *ThreadControllerImpl) Create(c *gin.Context) {
	threadRequest := web_thread.ThreadCreateRequest{}

	err := c.BindJSON(&threadRequest)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	threadRequest.IdUserFk = user
	threadRequest.Rating = 0

	threadResp := t.ThreadService.Create(c, threadRequest)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   threadResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Update Thread godoc
// @Summary User Update Existing Thread.
// @Description Digunakan oleh user untuk mengubah isi Thread / Posting yang dimiliki.
// @Tags Thread
// @Param Body body web_thread.ThreadCreateRequest true "the body to updating thread"
// @Param threadId path string true "thread id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_thread.ThreadResponse}
// @Router /thread/{threadId} [put]
func (t *ThreadControllerImpl) Update(c *gin.Context) {

	paramId := c.Param("threadId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Thread ID tidak boleh kosong"))
	}

	threadId , err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	threadRequest := web_thread.ThreadCreateRequest{}
	err = c.BindJSON(&threadRequest)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	threadRequest.IdUserFk = user

	threadResp := t.ThreadService.Update(c, threadRequest, threadId)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   threadResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Delete Thread godoc
// @Summary User Delete Existing Thread.
// @Description Digunakan oleh user untuk menghapus Thread / Posting yang dimiliki.
// @Tags Thread
// @Param threadId path string true "thread id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse
// @Router /thread/{threadId} [delete]
func (t *ThreadControllerImpl) Delete(c *gin.Context) {
	paramId := c.Param("threadId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Thread ID tidak boleh kosong"))
	}

	threadId , err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)

	t.ThreadService.Delete(c, user, threadId)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   "Berhasil hapus thread",
	}

	c.JSON(http.StatusOK, webResponse)

}

// Get User Thread godoc
// @Summary User Get Thread.
// @Description Digunakan oleh user untuk mencari thread tertentu berdasarkan Id.
// @Tags Thread
// @Param threadId path string true "thread id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=web_thread.ThreadResponse}
// @Router /thread/{threadId} [get]
func (t *ThreadControllerImpl) FindById(c *gin.Context) {
	paramId := c.Param("threadId")
	if paramId == ""{
		panic(exception.NewCustomValidationError("Thread ID tidak boleh kosong"))
	}

	threadId , err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	threadResp := t.ThreadService.FindById(c, threadId)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   threadResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Get All Personal Thread godoc
// @Summary User Get Personal Thread.
// @Description Digunakan oleh user untuk mendapatkan semua thread miliknya.
// @Tags Thread
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=[]web_thread.ThreadResponse}
// @Router /thread [get]
func (t *ThreadControllerImpl) FindAll(c *gin.Context) {

	bearerToken := c.GetHeader("Authorization")
	user, err :=helper.ExtractTokenData(bearerToken)
	helper.PanicIfError(err)


	threadResp := t.ThreadService.FindAll(c, user)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   threadResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

// Get All Thread godoc
// @Summary User Get All Thread.
// @Description Digunakan oleh user untuk mendapatkan seluruh Thread yang ada.
// @Tags Thread
// @Security BearerToken
// @Produce json
// @Success 200 {object} web.WebResponse{data=[]web_thread.ThreadResponse}
// @Router /thread/all [get]
func (t *ThreadControllerImpl) FindAllUnprotected(c *gin.Context) {

	threadResp := t.ThreadService.FindAllUnprotected(c)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   threadResp,
	}

	c.JSON(http.StatusOK, webResponse)
}

