package thread_controller

import "github.com/gin-gonic/gin"

type ThreadController interface {
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	FindById(c *gin.Context)
	FindAll(c *gin.Context)
	FindAllUnprotected(c *gin.Context)
}
