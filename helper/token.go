package helper

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	"strconv"
	"strings"
	"time"
)

func GenerateToken(userId int) (string, error) {
	token_lifespan, err := strconv.Atoi(Getenv("TOKEN_HOUR_LIFESPAN", "1"))

	PanicIfError(err)

	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["user"] = userId
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(token_lifespan)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(Getenv("API_SECRET", "")))

}

func TokenValid(bearerToken string) error {

	tokenString := ExtractToken(bearerToken)
	_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(Getenv("API_SECRET", "")), nil
	})

	if err != nil {
		return err
	}

	return nil
}

func ExtractToken(bearerToken string) string {
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}

	return ""
}

func ExtractTokenData(bearerToken string) (int, error) {
	userId := -1
	tokenString := ExtractToken(bearerToken)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(Getenv("API_SECRET", "")), nil
	})

	if err != nil {
		return userId , err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		userId, err = strconv.Atoi(fmt.Sprintf("%v", claims["user"]))

		if err != nil {
			return userId, err
		}

		return userId, nil
	}

	return userId, nil
}
