package user_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UserRepository interface {
	Register(c *gin.Context, db *gorm.DB,  user domain.User) domain.User
	UpdatePassword(c *gin.Context, db *gorm.DB, user domain.User) domain.User
	FindByUsername(c *gin.Context, db *gorm.DB, user domain.User) (domain.User, error)
	FindById(c *gin.Context, db *gorm.DB, userId int) (domain.User, error)
	VerifUser(c *gin.Context, db *gorm.DB, userId int, token string) error
	DeleteUser(c *gin.Context, db *gorm.DB, userId int)
}
