package user_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UserRepositoryImpl struct {

}

func NewUserRepository() UserRepository {
	return &UserRepositoryImpl{}
}

func (u *UserRepositoryImpl) Register(c *gin.Context, db *gorm.DB, user domain.User) domain.User {
	result := db.Create(&user)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return user
}

func (u *UserRepositoryImpl) UpdatePassword(c *gin.Context, db *gorm.DB, user domain.User) domain.User {
	result := db.Model(&domain.User{}).Where("username = ? or phone = ?", user.Username, user.Phone).Update("password", user.Password)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return user
}

func (u *UserRepositoryImpl) FindByUsername(c *gin.Context, db *gorm.DB, user domain.User) (domain.User, error) {
	result := db.First(&user, "username = ?", user.Username)

	if result.Error != nil {
		return user, errors.New("User is not found")
	}

	return user, nil
}

func (u *UserRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, userId int) (domain.User, error) {

	user := domain.User{}
	result := db.First(&user, userId)

	if result.Error != nil {
		return user, errors.New("User is not found")
	}

	return user, nil
}

func (u *UserRepositoryImpl) VerifUser(c *gin.Context, db *gorm.DB, userId int, token string) error {

	user := domain.User{}
	result := db.First(&user, userId)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	if user.VerifToken == token{
		result = db.Model(&domain.User{}).Where("id_user = ?", userId).Update("is_verified", 1)
		return nil

	}else{
		return errors.New("Gagal Verifikasi Akun")
	}
}

func (u *UserRepositoryImpl) DeleteUser(c *gin.Context, db *gorm.DB, userId int) {

	result := db.Delete(&domain.User{}, userId)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

