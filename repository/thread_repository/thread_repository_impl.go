package thread_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ThreadRepositoryImpl struct {
}


func NewThreadRepository() ThreadRepository {
	return &ThreadRepositoryImpl{}
}

func (t ThreadRepositoryImpl) Save(c *gin.Context, db *gorm.DB, thread domain.Thread) domain.Thread {
	result := db.Create(&thread)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return thread
}

func (t ThreadRepositoryImpl) Update(c *gin.Context, db *gorm.DB, thread domain.Thread) domain.Thread {
	result := db.Model(&thread).Updates(thread)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return thread
}

func (t ThreadRepositoryImpl) Delete(c *gin.Context, db *gorm.DB, threadId int) {
	result := db.Delete(&domain.Thread{}, threadId)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (t ThreadRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, threadId int) (domain.Thread, error) {

	thread := domain.Thread{}
	result := db.First(&thread, threadId)

	if result.Error != nil {
		return thread, errors.New("Thread tidak ditemukan")
	}

	return thread, nil
}

func (t ThreadRepositoryImpl) FindAll(c *gin.Context, db *gorm.DB, userId int) []domain.Thread {

	threads := []domain.Thread{}
	db.Where("id_user_fk = ?", userId).Find(&threads)

	return threads
}

func (t ThreadRepositoryImpl) FindAllUnprotected(c *gin.Context, db *gorm.DB) []domain.Thread {

	threads := []domain.Thread{}
	db.Find(&threads)

	return threads
}

