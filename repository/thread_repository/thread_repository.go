package thread_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ThreadRepository interface {
	Save(c *gin.Context, db *gorm.DB, thread domain.Thread) domain.Thread
	Update(c *gin.Context, db *gorm.DB, thread domain.Thread) domain.Thread
	Delete(c *gin.Context, db *gorm.DB, threadId int)
	FindById(c *gin.Context, db *gorm.DB, threadId int) (domain.Thread, error)
	FindAll(c *gin.Context, db *gorm.DB, userId int) []domain.Thread
	FindAllUnprotected(c *gin.Context, db *gorm.DB) []domain.Thread
}