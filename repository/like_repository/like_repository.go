package like_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LikeRepository interface {
	Save(c *gin.Context, db *gorm.DB, commentId, userId int)
	Delete(c *gin.Context, db *gorm.DB, commentId, userId int)
	Count(c *gin.Context, db *gorm.DB, commentId int) int
	FindById(c *gin.Context, db *gorm.DB, commentId, userId int) (domain.Like , error)
}