package like_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LikeRepositoryImpl struct {

}

func NewLikeRepository() LikeRepository {
	return &LikeRepositoryImpl{}
}

func (l *LikeRepositoryImpl) Save(c *gin.Context, db *gorm.DB, commentId, userId int) {
	result := db.Create(&domain.Like{
		IdCommentFk: commentId,
		IdUserFk:    userId,
	})

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (l *LikeRepositoryImpl) Delete(c *gin.Context, db *gorm.DB, commentId, userId int) {

	result := db.Where("id_user_fk = ? and id_comment_fk = ?", userId, commentId).Delete(domain.Like{})
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

}


func (l *LikeRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, commentId, userId int) (domain.Like, error) {
	like := domain.Like{}
	result := db.Where("id_user_fk = ? and id_comment_fk = ?", userId, commentId).First(&like)

	if result.Error != nil {
		return like, errors.New("Like is not found")
	}

	return like, nil
}

func (l *LikeRepositoryImpl) Count(c *gin.Context, db *gorm.DB, commentId int) int {

	likes := []domain.Like{}
	db.Where("id_comment_fk = ?", commentId).Find(&likes)
	return len(likes)

}


