package saved_thread_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type SavedThreadRepositoryImpl struct {

}

func NewSavedThreadRepository() SavedThreadRepository {
	return &SavedThreadRepositoryImpl{}
}

func (s *SavedThreadRepositoryImpl) SaveThread(c *gin.Context, db *gorm.DB, userId, threadId int) {
	result := db.Create(&domain.SavedThread{
		IdUserFk:      userId,
		IdThreadFk:    threadId,
	})

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (s *SavedThreadRepositoryImpl) DeleteSavedThread(c *gin.Context, db *gorm.DB, userId, threadId int) {

	result := db.Where("id_user_fk = ? and id_thread_fk = ?", userId, threadId).Delete(&domain.SavedThread{})

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (s *SavedThreadRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, userId, threadId int) (domain.SavedThread, error) {

	saved := domain.SavedThread{}
	result := db.Where("id_user_fk = ? and id_thread_fk = ?", userId, threadId).First(&saved)

	if result.Error != nil {
		return saved, errors.New("Saved thread is not found")
	}

	return saved, nil
}

func (s *SavedThreadRepositoryImpl) FindAllSavedThread(c *gin.Context, db *gorm.DB, userId int) []domain.SavedThread {
	saveds := []domain.SavedThread{}

	db.Where("id_user_fk = ?", userId).Find(&saveds)

	return saveds
}

