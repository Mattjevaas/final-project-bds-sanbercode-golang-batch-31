package saved_thread_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type SavedThreadRepository interface {
	SaveThread(c *gin.Context, db *gorm.DB, userId, threadId int)
	DeleteSavedThread(c *gin.Context, db *gorm.DB, userId, threadId int)
	FindById(c *gin.Context, db *gorm.DB, userId, threadId int) (domain.SavedThread, error)
	FindAllSavedThread(c *gin.Context, db *gorm.DB, userId int) []domain.SavedThread
}