package rating_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type RatingRepositoryImpl struct {

}

func NewRatingRepository() RatingRepository {
	return &RatingRepositoryImpl{}
}

func (r *RatingRepositoryImpl) Save(c *gin.Context, db *gorm.DB, rating domain.Rating) domain.Rating {
	result := db.Create(&rating)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return rating
}

func (r *RatingRepositoryImpl) Update(c *gin.Context, db *gorm.DB, rating domain.Rating) domain.Rating {
	result := db.Model(&rating).Updates(rating)

	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

	return rating
}

func (r *RatingRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, userId, threadId int) (domain.Rating, error) {

	rating := domain.Rating{}
	result := db.Where("id_user_fk = ? and id_thread_fk = ?", userId, threadId).First(&rating)

	if result.Error != nil {
		return rating, errors.New("Rating is not found")
	}

	return rating, nil
}

func (r *RatingRepositoryImpl) CalculateOverall(c *gin.Context, db *gorm.DB, threadId int) float64 {

	ratings := []domain.Rating{}

	result := db.Where("id_thread_fk = ?", threadId).Find(&ratings)

	if result.Error == nil {
		sum := 0

		if len(ratings) != 0 {
			for _ , data := range ratings{
				sum += data.Rating
			}

			length := result.RowsAffected
			return float64(int64(sum) / length)
		}
	}

	return 0
}
