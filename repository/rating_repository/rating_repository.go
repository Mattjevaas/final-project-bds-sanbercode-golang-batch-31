package rating_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type RatingRepository interface {
	Save(c *gin.Context, db *gorm.DB, rating domain.Rating) domain.Rating
	Update(c *gin.Context, db *gorm.DB, rating domain.Rating) domain.Rating
	FindById(c *gin.Context, db *gorm.DB, userId, threadId int) (domain.Rating, error)
	CalculateOverall(c *gin.Context, db *gorm.DB, threadId int) float64
}

