package image_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ImageRepositoryImpl struct {

}

func NewImageRepository() ImageRepository {
	return &ImageRepositoryImpl{}
}

func (i *ImageRepositoryImpl) Add(c *gin.Context, db *gorm.DB, image domain.Image) {
	result := db.Create(&image)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (i *ImageRepositoryImpl) Delete(c *gin.Context, db *gorm.DB, imageId int) {
	result := db.Delete(&domain.Image{}, imageId)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (i *ImageRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, imageId int) (domain.Image, error) {
	image := domain.Image{}
	result := db.First(&image, imageId)

	if result.Error != nil {
		return image, errors.New("Image url is not found")
	}

	return image, nil
}

func (i *ImageRepositoryImpl) FindAll(c *gin.Context, db *gorm.DB, threadId int) []domain.Image {
	images := []domain.Image{}
	db.Where("id_thread_fk = ?", threadId).Find(&images)

	return images
}



