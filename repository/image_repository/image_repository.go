package image_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ImageRepository interface {
	Add(c *gin.Context, db *gorm.DB, image domain.Image)
	Delete(c *gin.Context, db *gorm.DB, imageId int)
	FindById(c *gin.Context, db *gorm.DB, imageId int) (domain.Image, error)
	FindAll(c *gin.Context, db *gorm.DB, threadId int) []domain.Image
}
