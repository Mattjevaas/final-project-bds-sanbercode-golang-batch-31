package comment_repository

import (
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CommentRepository interface {
	Add(c *gin.Context, db *gorm.DB, comment domain.Comment)
	Delete(c *gin.Context, db *gorm.DB, userId, commentId int)
	FindById(c *gin.Context, db *gorm.DB, userId, commentId int) (domain.Comment, error)
	FindByIdOnly(c *gin.Context, db *gorm.DB, commentId int) (domain.Comment, error)
	FindAll(c *gin.Context, db *gorm.DB, threadId int) [](domain.Comment)
}
