package comment_repository

import (
	"errors"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CommentRepositoryImpl struct {

}

func NewCommentRepository() CommentRepository {
	return &CommentRepositoryImpl{}
}

func (co *CommentRepositoryImpl) Add(c *gin.Context, db *gorm.DB, comment domain.Comment) {
	result := db.Create(&comment)
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}
}

func (co *CommentRepositoryImpl) Delete(c *gin.Context, db *gorm.DB, userId, commentId int) {

	result := db.Where("id_user_fk = ? and id_comment = ?", userId, commentId).Delete(domain.Comment{})
	if result.Error != nil {
		helper.PanicIfError(result.Error)
	}

}

func (co *CommentRepositoryImpl) FindById(c *gin.Context, db *gorm.DB, userId, commentId int) (domain.Comment, error) {
	comment := domain.Comment{}
	result := db.Where("id_user_fk = ? and id_comment = ?", userId, commentId).First(&comment)

	if result.Error != nil {
		return comment, errors.New("Comment is not found")
	}

	return comment, nil
}

func (co *CommentRepositoryImpl) FindByIdOnly(c *gin.Context, db *gorm.DB, commentId int) (domain.Comment, error) {
	comment := domain.Comment{}
	result := db.Where("id_comment = ?", commentId).First(&comment)

	if result.Error != nil {
		return comment, errors.New("Comment is not found")
	}

	return comment, nil
}

func (co *CommentRepositoryImpl) FindAll(c *gin.Context, db *gorm.DB, threadId int) []domain.Comment {
	comment := []domain.Comment{}
	db.Where("id_thread_fk = ?", threadId).Find(&comment)

	return comment
}



