package app

import (
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/driver/postgres"
	"time"
)

func NewDB() *gorm.DB{

	db_uname := helper.Getenv("DATABASE_USERNAME", "")
	db_pass := helper.Getenv("DATABASE_PASSWORD", "")

	db_url := helper.Getenv("DATABASE_HOST", "")
	db_port := helper.Getenv("DATABASE_PORT", "")
	db_name := helper.Getenv("DATABASE_NAME", "")

	//state := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", db_uname, db_pass, db_url, db_port, db_name)
	state := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", db_url, db_uname, db_pass, db_name, db_port)

	db, err := gorm.Open(postgres.Open(state), &gorm.Config{})

	helper.PanicIfError(err)

	sqlDB, err := db.DB()

	helper.PanicIfError(err)

	sqlDB.SetMaxIdleConns(5)
	sqlDB.SetMaxOpenConns(20)
	sqlDB.SetConnMaxLifetime(60 * time.Minute)

	err = db.AutoMigrate(
		&domain.User{},
		&domain.Thread{},
		&domain.Comment{},
		&domain.Rating{},
		&domain.Like{},
		&domain.Image{},
		&domain.SavedThread{},
	)

	helper.PanicIfError(err)

	return db
}
