package app

import (
	"final-project-bds-sanbercode-golang-batch-31/controller/comment_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/image_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/like_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/rating_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/saved_thread_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/thread_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/user_controller"
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/middleware"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	"gorm.io/gorm"
)

func NewRouter(
	db *gorm.DB,
	controller user_controller.UserController,
	ratingController rating_controller.RatingController,
	savedThreadController saved_thread_controller.SavedThreadController,
	likeController like_controller.LikeController,
	commentController comment_controller.CommentController,
	imageController image_controller.ImageController,
	threadController thread_controller.ThreadController,
) *gin.Engine{

	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.Use(gin.CustomRecovery(exception.ErrorHandler))

	r.GET("/", func(context *gin.Context) {
		context.Writer.Write([]byte("<h1> WELCOME TO TECH BLOG API by Johanes Wiku Sakti</h1>"))
	})

	api := r.Group("/api/v1")
	{
		api.POST("/login", controller.Login)
		api.POST("/register", controller.Register)
		api.POST("/reset-password", controller.UpdatePassword)
		api.GET("/verification/:id/:token", controller.Verif)
		api.GET("/resend-verification/:username", controller.ResendToken)

		rating := api.Group("/rating")
		{
			rating.Use(middleware.JwtAuthMiddleware())
			rating.POST("/", ratingController.Rate)
			rating.PUT("/", ratingController.ChangeRate)
		}

		saved := api.Group("/saved-thread")
		{
			saved.Use(middleware.JwtAuthMiddleware())
			saved.POST("/save/:threadId", savedThreadController.SaveThread)
			saved.DELETE("/delete/:threadId", savedThreadController.DeleteSavedThread)
			saved.GET("/", savedThreadController.FindAllSavedThread)
		}

		like := api.Group("/like")
		{
			like.Use(middleware.JwtAuthMiddleware())
			like.POST("/:commentId", likeController.Like)
			like.DELETE("/:commentId", likeController.Unlike)
		}

		image := api.Group("/image")
		{
			image.Use(middleware.JwtAuthMiddleware())
			image.POST("/", imageController.Add)
			image.DELETE("/:imageId", imageController.Delete)
		}

		comment := api.Group("/comment")
		{
			comment.Use(middleware.JwtAuthMiddleware())
			comment.POST("/", commentController.AddComment)
			comment.DELETE("/:commentId", commentController.Delete)
		}

		api.GET("/thread/all", threadController.FindAllUnprotected)
		thread := api.Group("/thread")
		{
			thread.Use(middleware.JwtAuthMiddleware())
			thread.GET("/", threadController.FindAll)
			thread.GET("/:threadId", threadController.FindById)
			thread.POST("/", threadController.Create)
			thread.PUT("/:threadId", threadController.Update)
			thread.DELETE("/:threadId", threadController.Delete)
		}
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}

