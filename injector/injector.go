//go:build wireinject
// +build wireinject

package injector

import (
	"final-project-bds-sanbercode-golang-batch-31/app"
	"final-project-bds-sanbercode-golang-batch-31/controller/comment_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/image_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/like_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/rating_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/saved_thread_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/thread_controller"
	"final-project-bds-sanbercode-golang-batch-31/controller/user_controller"
	"final-project-bds-sanbercode-golang-batch-31/repository/comment_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/image_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/like_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/rating_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/saved_thread_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/user_repository"
	"final-project-bds-sanbercode-golang-batch-31/service/comment_service"
	"final-project-bds-sanbercode-golang-batch-31/service/image_service"
	"final-project-bds-sanbercode-golang-batch-31/service/like_service"
	"final-project-bds-sanbercode-golang-batch-31/service/rating_service"
	"final-project-bds-sanbercode-golang-batch-31/service/saved_thread_service"
	"final-project-bds-sanbercode-golang-batch-31/service/thread_service"
	"final-project-bds-sanbercode-golang-batch-31/service/user_service"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/wire"
)

func InitializeServer() *gin.Engine{
	wire.Build(
		app.NewDB,
		app.NewRouter,
		validator.New,
		user_controller.NewUserController,
		user_service.NewUserService,
		user_repository.NewUserRepository,
		rating_controller.NewRatingController,
		rating_service.NewRatingService,
		rating_repository.NewRatingRepository,
		saved_thread_controller.NewSavedThreadController,
		saved_thread_service.NewSavedThreadService,
		saved_thread_repository.NewSavedThreadRepository,
		like_controller.NewLikeController,
		like_service.NewLikeService,
		like_repository.NewLikeRepository,
		thread_controller.NewThreadController,
		thread_service.NewThreadService,
		thread_repository.NewThreadRepository,
		image_controller.NewImageController,
		image_service.NewImageService,
		image_repository.NewImageRepository,
		comment_controller.NewCommentController,
		comment_service.NewCommentService,
		comment_repository.NewCommentRepository,
	)
	return nil
}
