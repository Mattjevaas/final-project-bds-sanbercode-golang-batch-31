package domain

import "time"

type User struct {
	IdUser int                 `gorm:"primaryKey"`
	Username string            `gorm:"unique;not null"`
	Password string            `gorm:"not null"`
	Phone string               `gorm:"unique;not null"`
	IsVerified int             `gorm:"not null;default:0"`
	VerifToken string
	CreatedAt time.Time        `gorm:"autoCreateTime"`
	UpdatedAt time.Time        `gorm:"autoUpdateTime"`
	Threads []Thread           `gorm:"foreignKey:id_user_fk;constraint:OnDelete:CASCADE;"`
	Ratings []Rating           `gorm:"foreignKey:id_user_fk;constraint:OnDelete:CASCADE;"`
	SavedThreads []SavedThread `gorm:"foreignKey:id_user_fk;constraint:OnDelete:CASCADE;"`
	Likes []Like 			   `gorm:"foreignKey:id_user_fk;constraint:OnDelete:CASCADE;"`
	Comment []Comment          `gorm:"foreignKey:id_user_fk;constraint:OnDelete:CASCADE;"`
}