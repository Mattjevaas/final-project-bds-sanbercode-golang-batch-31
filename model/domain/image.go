package domain

import "time"

type Image struct {
	IdImage int 	    `gorm:"primaryKey"`
	ImageUrl string		`gorm:"not null"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	IdThreadFk int
}
