package domain

import "time"

type Like struct {
	IdLike int 	    `gorm:"primaryKey"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	IdCommentFk int
	IdUserFk int
}
