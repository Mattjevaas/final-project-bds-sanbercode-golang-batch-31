package domain

import "time"

type Comment struct {
	IdComment int 	    `gorm:"primaryKey"`
	Content string		`gorm:"type:text;not null"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	IdThreadFk int
	IdUserFk int
	Likes []Like        `gorm:"foreignKey:id_comment_fk;constraint:OnDelete:CASCADE;"`
}
