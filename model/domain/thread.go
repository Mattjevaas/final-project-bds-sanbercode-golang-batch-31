package domain

import (
	"time"
)

type Thread struct {
	IdThread int 	    `gorm:"primaryKey"`
	Title string		`gorm:"not null"`
	Content string		`gorm:"type:text;not null"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	IdUserFk int
	Ratings []Rating          `gorm:"foreignKey:id_thread_fk;constraint:OnDelete:CASCADE;"`
	Images []Image            `gorm:"foreignKey:id_thread_fk;constraint:OnDelete:CASCADE;"`
	Comments []Comment        `gorm:"foreignKey:id_thread_fk;constraint:OnDelete:CASCADE;"`
	SavedThread []SavedThread `gorm:"foreignKey:id_thread_fk;constraint:OnDelete:CASCADE;"`
}