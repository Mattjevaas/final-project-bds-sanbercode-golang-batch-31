package domain

import "time"

type Rating struct {
	IdRating int 	    `gorm:"primaryKey"`
	Rating int			`gorm:"not null"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
	IdUserFk int
	IdThreadFk int
}
