package domain

import "time"

type SavedThread struct {
	IdSavedThread int 	    `gorm:"primaryKey"`
	CreatedAt time.Time 	`gorm:"autoCreateTime"`
	UpdatedAt time.Time 	`gorm:"autoUpdateTime"`
	IdUserFk int
	IdThreadFk int
}
