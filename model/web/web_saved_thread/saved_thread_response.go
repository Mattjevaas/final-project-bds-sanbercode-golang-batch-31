package web_saved_thread

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_thread"
)

type SavedThreadResponse struct {
	IdSavedThread int `json:"id_saved_thread"`
	Thread web_thread.ThreadResponse `json:"thread"`
}
