package web_image

type ImageRequest struct {
	IdThreadFk int `json:"id_thread" validate:"required"`
	ImageUrl string `json:"image_url" validate:"required,url"`
	IdUserFk int `json:"-" validate:"required"`
}
