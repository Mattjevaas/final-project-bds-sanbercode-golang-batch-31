package web_image

type ImageResponse struct {
	IdImage int `json:"id_image"`
	ImageUrl string `json:"image_url"`
}
