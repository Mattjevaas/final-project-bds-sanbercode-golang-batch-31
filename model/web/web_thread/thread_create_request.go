package web_thread

type ThreadCreateRequest struct {
	Title string `json:"title" validate:"required"`
	Content string `json:"content" validate:"required"`
	IdUserFk int `json:"-" validate:"required"`
	Rating float64 `json:"-"`
}
