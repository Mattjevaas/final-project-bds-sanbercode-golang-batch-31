package web_thread

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"time"
)

type ThreadResponse struct {
	IdThread int `json:"id_thread"`
	IdUserFk int `json:"id_author"`
	Title string `json:"title"`
	Content string `json:"content"`
	Rating float64 `json:"rating"`
	ImagesUrl []web_image.ImageResponse `json:"images_url"`
	Comments []web_comment.CommentResponse `json:"comments"`
	Created_at time.Time `json:"created_at"`
	Updated_at time.Time `json:"updated_at"`
}

