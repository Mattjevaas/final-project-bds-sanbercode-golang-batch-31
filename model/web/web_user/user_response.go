package web_user

// UserResponse
type UserResponse struct {
	IdUser int `json:"id_user"`
	Username string `json:"username"`
	Phone string `json:"phone"`
}
