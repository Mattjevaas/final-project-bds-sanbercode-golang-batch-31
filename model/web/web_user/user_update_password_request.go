package web_user

// UserUpdatePasswordRequest
type UserUpdatePasswordRequest struct {
	Username string `json:"username" validate:"required"`
	Phone string `json:"-"`
	OldPassword string `json:"old_password" validate:"required,min=5"`
	NewPassword string `json:"new_password" validate:"required,min=5"`
}
