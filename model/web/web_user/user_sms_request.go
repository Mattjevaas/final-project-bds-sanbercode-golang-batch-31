package web_user

// UserSMSRequest
type UserSMSRequest struct {
	IdUser int `json:"id_user" validate:"required"`
	Phone string `json:"phone" validate:"required"`
	VerifToken string `json:"verif_token" validate:"required"`
}
