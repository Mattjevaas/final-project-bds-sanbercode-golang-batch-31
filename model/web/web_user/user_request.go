package web_user

// UserRequest
type UserRequest struct {
	Username string `json:"username"`
	Phone string `json:"phone"`
}
