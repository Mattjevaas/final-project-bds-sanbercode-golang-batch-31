package web_user

// UserRegisterRequest
type UserRegisterRequest struct {
	Username string `json:"username" validate:"required,min=5,alphanum"`
	Phone string `json:"phone" validate:"required"`
	Password string `json:"password" validate:"required,min=5"`
}
