package web_user

// UserLoginResponse
type UserLoginResponse struct {
	IdUser int `json:"id_user"`
	Username string `json:"username"`
	Phone string `json:"phone"`
	Token string `json:"token"`
	ExpireTime string `json:"expire_time"`
}
