package web_rating


type RatingCreateRequest struct {
	Rating int `json:"rating" validate:"required,min=1,max=5"`
	IdUserFk int `json:"-" validate:"required"`
	IdThreadFk int `json:"id_thread" validate:"required"`
}
