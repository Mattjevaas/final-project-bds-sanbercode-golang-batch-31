package web_comment

type CommentCreateRequest struct {
	Content string `json:"content" validate:"required"`
	IdThreadFk int `json:"id_thread" validate:"required"`
	IdUserFk int   `json:"-"`
}
