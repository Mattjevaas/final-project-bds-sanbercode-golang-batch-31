package web_comment

import "time"

type CommentResponse struct {
	IdComment int `json:"id_comment"`
	IdUserFk int `json:"id_user"`
	Content string `json:"content"`
	Likes int `json:"likes"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
