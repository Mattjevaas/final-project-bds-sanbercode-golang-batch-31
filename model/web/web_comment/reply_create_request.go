package web_comment

type ReplyCreateRequest struct {
	Content string `json:"content" validate:"required"`
	IdCommentFk int `json:"id_comment" validate:"required"`
	IdUserFk int `json:"-" validate:"required"`
}
