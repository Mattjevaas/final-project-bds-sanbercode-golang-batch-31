package exception

type PasswordNotMatchError struct {
	Error string
}

func NewPasswordNotMatchError(error string) PasswordNotMatchError {
	return PasswordNotMatchError{Error: error}
}

