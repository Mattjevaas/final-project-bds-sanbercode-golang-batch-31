package exception

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

func ErrorHandler(c *gin.Context, recovered interface{}) {

	if customValidationErrors(c, recovered){
		return
	}

	if validationErrors(c, recovered){
		return
	}

	if errorNotFound(c, recovered){
		return
	}

	if errorPasswordNotMatch(c, recovered){
		return
	}

	internalServerError(c, recovered)
}

func customValidationErrors(c *gin.Context, recovered interface{}) bool {

	rec, ok := recovered.(CustomValidationError)

	if ok {
		webResponse := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "BAD REQUEST",
			Data:   rec.Error,
		}

		c.JSON(http.StatusBadRequest, webResponse)
		return true
	}

	return false
}

func validationErrors(c *gin.Context, recovered interface{}) bool {

	rec, ok := recovered.(validator.ValidationErrors)

	if ok {
		webResponse := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "BAD REQUEST",
			Data:   rec.Error(),
		}

		c.JSON(http.StatusBadRequest, webResponse)
		return true
	}

	return false
}

func errorPasswordNotMatch(c *gin.Context, recovered interface{}) bool {
	rec, ok := recovered.(PasswordNotMatchError)

	if ok {
		webResponse := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "WRONG PASSWORD",
			Data:   rec,
		}

		c.JSON(http.StatusBadRequest, webResponse)
		return true
	}

	return false
}

func errorNotFound(c *gin.Context, recovered interface{}) bool {
	rec, ok := recovered.(NotFoundError)

	if ok {
		webResponse := web.WebResponse{
			Code:   http.StatusNotFound,
			Status: "NOT FOUND",
			Data:   rec,
		}

		c.JSON(http.StatusBadRequest, webResponse)
		return true
	}

	return false
}

func internalServerError(c *gin.Context, recovered interface{}){

	webResponse := web.WebResponse{
		Code:   http.StatusInternalServerError,
		Status: "INTERNAL SERVER ERROR",
		Data:   recovered,
	}

	c.JSON(http.StatusInternalServerError, webResponse)
}
