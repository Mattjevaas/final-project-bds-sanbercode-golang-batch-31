package exception

type CustomValidationError struct {
	Error string
}

func NewCustomValidationError(error string) CustomValidationError {
	return CustomValidationError{Error: error}
}
