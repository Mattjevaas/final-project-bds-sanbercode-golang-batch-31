package like_service

import "github.com/gin-gonic/gin"

type LikeService interface {
	Like(c *gin.Context, commentId, userId int)
	Unlike(c *gin.Context, commentId, userId int)
	Count(c *gin.Context, commentId int) int
}
