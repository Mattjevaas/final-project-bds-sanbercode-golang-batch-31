package like_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/repository/comment_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/like_repository"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LikeServiceImpl struct {
	LikeRepository like_repository.LikeRepository
	CommentRepository comment_repository.CommentRepository
}


func NewLikeService(likeRepository like_repository.LikeRepository, commentrepository comment_repository.CommentRepository) LikeService {
	return &LikeServiceImpl{LikeRepository: likeRepository, CommentRepository: commentrepository}
}

func (l *LikeServiceImpl) Like(c *gin.Context, commentId, userId int) {
	db := c.MustGet("db").(*gorm.DB)

	_, err := l.CommentRepository.FindByIdOnly(c, db, commentId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	_, err = l.LikeRepository.FindById(c, db, commentId, userId)
	if err == nil {
		panic(exception.NewCustomValidationError("Comment sudah dilike "))
	}

	l.LikeRepository.Save(c, db , commentId, userId)
}

func (l *LikeServiceImpl) Unlike(c *gin.Context, commentId, userId int) {
	db := c.MustGet("db").(*gorm.DB)

	_, err := l.LikeRepository.FindById(c, db, commentId, userId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	l.LikeRepository.Delete(c, db , commentId, userId)
}

func (l *LikeServiceImpl) Count(c *gin.Context, commentId int) int {
	db := c.MustGet("db").(*gorm.DB)

	return l.LikeRepository.Count(c, db , commentId)
}

