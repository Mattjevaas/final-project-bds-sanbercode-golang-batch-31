package thread_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_thread"
	"github.com/gin-gonic/gin"
)

type ThreadService interface {
	Create(c *gin.Context, request web_thread.ThreadCreateRequest) web_thread.ThreadResponse
	Update(c *gin.Context, request web_thread.ThreadCreateRequest, threadId int) web_thread.ThreadResponse
	Delete(c *gin.Context, threadId, userId int)
	FindById(c *gin.Context, threadId int) web_thread.ThreadResponse
	FindAll(c *gin.Context, userId int) []web_thread.ThreadResponse
	FindAllUnprotected(c *gin.Context) []web_thread.ThreadResponse
}
