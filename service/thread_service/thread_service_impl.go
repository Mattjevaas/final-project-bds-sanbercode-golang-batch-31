package thread_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_thread"
	"final-project-bds-sanbercode-golang-batch-31/repository/comment_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/image_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/like_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/rating_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type ThreadServiceImpl struct {
	ThreadRepository thread_repository.ThreadRepository
	ImageRepository image_repository.ImageRepository
	RatingRepository rating_repository.RatingRepository
	CommentRepository comment_repository.CommentRepository
	LikeRepository like_repository.LikeRepository
	Validate *validator.Validate
}

func NewThreadService(
	threadRepository thread_repository.ThreadRepository,
	imageRepository image_repository.ImageRepository,
	ratingRepository rating_repository.RatingRepository,
	commentRepository comment_repository.CommentRepository,
	likeRepository like_repository.LikeRepository,
	validate *validator.Validate,
) ThreadService {
	return &ThreadServiceImpl{
		ThreadRepository: threadRepository,
		ImageRepository: imageRepository,
		RatingRepository: ratingRepository,
		CommentRepository: commentRepository,
		LikeRepository: likeRepository,
		Validate: validate,
	}
}

func (t *ThreadServiceImpl) Create(c *gin.Context, request web_thread.ThreadCreateRequest) web_thread.ThreadResponse {
	err := t.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)


	thread := domain.Thread{
		Title:       request.Title,
		Content:     request.Content,
		IdUserFk:    request.IdUserFk,
	}

	thread = t.ThreadRepository.Save(c, db, thread)

	threadResp := web_thread.ThreadResponse{
		IdThread:   thread.IdThread,
		IdUserFk:   thread.IdUserFk,
		Title:      thread.Title,
		Content:    thread.Content,
		Rating:     0,
		ImagesUrl:  []web_image.ImageResponse{},
		Comments:  	[]web_comment.CommentResponse{},
		Created_at: thread.CreatedAt,
		Updated_at: thread.UpdatedAt,
	}

	return threadResp
}

func (t *ThreadServiceImpl) Update(c *gin.Context, request web_thread.ThreadCreateRequest, threadId int) web_thread.ThreadResponse {
	err := t.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)


	thread, err := t.ThreadRepository.FindById(c, db, threadId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	if thread.IdUserFk != request.IdUserFk{
		panic(exception.NewCustomValidationError("Thread ini bukan milik anda"))
	}

	thread = domain.Thread{
		IdThread: threadId,
		Title:    request.Title,
		Content:  request.Content,
		IdUserFk: request.IdUserFk,
	}

	thread = t.ThreadRepository.Update(c, db, thread)

	imagesResponse := make(chan []web_image.ImageResponse)
	images := t.ImageRepository.FindAll(c, db, thread.IdThread)

	go func(c chan []web_image.ImageResponse){
		imagesRes := []web_image.ImageResponse{}
		for _, image := range images {
			temp := web_image.ImageResponse{
				IdImage:   image.IdImage,
				ImageUrl:  image.ImageUrl,
			}
			imagesRes = append( imagesRes, temp)
		}

		c <- imagesRes
	}(imagesResponse)

	commentResponse := make(chan []web_comment.CommentResponse)
	comments := t.CommentRepository.FindAll(c, db, thread.IdThread)

	go func(ch chan []web_comment.CommentResponse){
		commentRes := []web_comment.CommentResponse{}
		for _, comment := range comments {

			like := t.LikeRepository.Count(c, db, comment.IdComment)

			temp := web_comment.CommentResponse{
				IdComment: comment.IdComment,
				IdUserFk: comment.IdUserFk,
				Content:   comment.Content,
				Likes:     like,
				CreatedAt: comment.CreatedAt,
				UpdatedAt: comment.UpdatedAt,
			}
			commentRes = append(commentRes, temp)
		}

		ch <- commentRes
	}(commentResponse)

	rating := t.RatingRepository.CalculateOverall(c, db, thread.IdThread)

	threadResp := web_thread.ThreadResponse{
		IdThread:  thread.IdThread,
		IdUserFk:   thread.IdUserFk,
		Title:     thread.Title,
		Content:   thread.Content,
		Rating:    rating,
		ImagesUrl: <- imagesResponse,
		Comments: <- commentResponse,
		Created_at: thread.CreatedAt,
		Updated_at: thread.UpdatedAt,
	}

	return threadResp
}

func (t *ThreadServiceImpl) Delete(c *gin.Context, userId, threadId int) {
	db := c.MustGet("db").(*gorm.DB)

	thread, err := t.ThreadRepository.FindById(c, db, threadId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	if thread.IdUserFk != userId{
		panic(exception.NewCustomValidationError("Thread ini bukan milik anda"))
	}

	t.ThreadRepository.Delete(c, db, threadId)
}

func (t *ThreadServiceImpl) FindById(c *gin.Context, threadId int) web_thread.ThreadResponse {
	db := c.MustGet("db").(*gorm.DB)

	thread, err := t.ThreadRepository.FindById(c, db, threadId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	imagesResponse := make(chan []web_image.ImageResponse)
	images := t.ImageRepository.FindAll(c, db, thread.IdThread)

	go func(c chan []web_image.ImageResponse){
		imagesRes := []web_image.ImageResponse{}
		for _, image := range images {
			temp := web_image.ImageResponse{
				IdImage:   image.IdImage,
				ImageUrl:  image.ImageUrl,
			}
			imagesRes = append( imagesRes, temp)
		}

		c <- imagesRes
	}(imagesResponse)

	commentResponse := make(chan []web_comment.CommentResponse)
	comments := t.CommentRepository.FindAll(c, db, thread.IdThread)

	go func(ch chan []web_comment.CommentResponse){
		commentRes := []web_comment.CommentResponse{}
		for _, comment := range comments {

			like := t.LikeRepository.Count(c, db, comment.IdComment)

			temp := web_comment.CommentResponse{
				IdComment: comment.IdComment,
				IdUserFk: comment.IdUserFk,
				Content:   comment.Content,
				Likes:     like,
				CreatedAt: comment.CreatedAt,
				UpdatedAt: comment.UpdatedAt,
			}
			commentRes = append(commentRes, temp)
		}

		ch <- commentRes
	}(commentResponse)

	rating := t.RatingRepository.CalculateOverall(c, db, thread.IdThread)

	threadResp := web_thread.ThreadResponse{
		IdThread:   thread.IdThread,
		IdUserFk:   thread.IdUserFk,
		Title:      thread.Title,
		Content:    thread.Content,
		Rating:     rating,
		ImagesUrl:  <- imagesResponse,
		Comments:   <- commentResponse,
		Created_at: thread.CreatedAt,
		Updated_at: thread.UpdatedAt,
	}

	return threadResp
}

func (t *ThreadServiceImpl) FindAll(c *gin.Context, userId int) []web_thread.ThreadResponse {
	db := c.MustGet("db").(*gorm.DB)


	threads := t.ThreadRepository.FindAll(c, db, userId)
	threadResponses := []web_thread.ThreadResponse{}

	if len(threads) != 0 {
		for _, thread := range threads{
			imagesResponse := make(chan []web_image.ImageResponse)
			images := t.ImageRepository.FindAll(c, db, thread.IdThread)

			go func(c chan []web_image.ImageResponse){
				imagesRes := []web_image.ImageResponse{}
				for _, image := range images {
					temp := web_image.ImageResponse{
						IdImage:   image.IdImage,
						ImageUrl:  image.ImageUrl,
					}
					imagesRes = append( imagesRes, temp)
				}

				c <- imagesRes
			}(imagesResponse)

			commentResponse := make(chan []web_comment.CommentResponse)
			comments := t.CommentRepository.FindAll(c, db, thread.IdThread)

			go func(ch chan []web_comment.CommentResponse){
				commentRes := []web_comment.CommentResponse{}
				for _, comment := range comments {

					like := t.LikeRepository.Count(c, db, comment.IdComment)

					temp := web_comment.CommentResponse{
						IdComment: comment.IdComment,
						IdUserFk: comment.IdUserFk,
						Content:   comment.Content,
						Likes:     like,
						CreatedAt: comment.CreatedAt,
						UpdatedAt: comment.UpdatedAt,
					}
					commentRes = append(commentRes, temp)
				}

				ch <- commentRes
			}(commentResponse)

			rating := t.RatingRepository.CalculateOverall(c, db, thread.IdThread)

			threadResp := web_thread.ThreadResponse{
				IdThread:   thread.IdThread,
				IdUserFk:   thread.IdUserFk,
				Title:      thread.Title,
				Content:    thread.Content,
				Rating:     rating,
				ImagesUrl:  <- imagesResponse,
				Comments:   <- commentResponse,
				Created_at: thread.CreatedAt,
				Updated_at: thread.UpdatedAt,
			}
			threadResponses = append(threadResponses, threadResp)
		}
	}

	return threadResponses
}

func (t *ThreadServiceImpl) FindAllUnprotected(c *gin.Context) []web_thread.ThreadResponse {
	db := c.MustGet("db").(*gorm.DB)


	threads := t.ThreadRepository.FindAllUnprotected(c, db)
	threadResponses := []web_thread.ThreadResponse{}

	if len(threads) != 0 {
		for _, thread := range threads{
			imagesResponse := make(chan []web_image.ImageResponse)
			images := t.ImageRepository.FindAll(c, db, thread.IdThread)

			go func(c chan []web_image.ImageResponse){
				imagesRes := []web_image.ImageResponse{}
				for _, image := range images {
					temp := web_image.ImageResponse{
						IdImage:   image.IdImage,
						ImageUrl:  image.ImageUrl,
					}
					imagesRes = append( imagesRes, temp)
				}

				c <- imagesRes
			}(imagesResponse)

			commentResponse := make(chan []web_comment.CommentResponse)
			comments := t.CommentRepository.FindAll(c, db, thread.IdThread)

			go func(ch chan []web_comment.CommentResponse){
				commentRes := []web_comment.CommentResponse{}
				for _, comment := range comments {

					like := t.LikeRepository.Count(c, db, comment.IdComment)

					temp := web_comment.CommentResponse{
						IdComment: comment.IdComment,
						IdUserFk: comment.IdUserFk,
						Content:   comment.Content,
						Likes:     like,
						CreatedAt: comment.CreatedAt,
						UpdatedAt: comment.UpdatedAt,
					}
					commentRes = append(commentRes, temp)
				}

				ch <- commentRes
			}(commentResponse)

			rating := t.RatingRepository.CalculateOverall(c, db, thread.IdThread)

			threadResp := web_thread.ThreadResponse{
				IdThread:   thread.IdThread,
				IdUserFk:   thread.IdUserFk,
				Title:      thread.Title,
				Content:    thread.Content,
				Rating:     rating,
				ImagesUrl:  <- imagesResponse,
				Comments:   <- commentResponse,
				Created_at: thread.CreatedAt,
				Updated_at: thread.UpdatedAt,
			}
			threadResponses = append(threadResponses, threadResp)
		}
	}

	return threadResponses
}
