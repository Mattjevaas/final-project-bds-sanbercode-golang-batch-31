package user_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_user"
	"github.com/gin-gonic/gin"
)

type UserService interface {
	Login(c *gin.Context, request web_user.UserLoginRequest) web_user.UserLoginResponse
	Register(c *gin.Context, request web_user.UserRegisterRequest) web_user.UserResponse
	UpdatePassword(c *gin.Context, request web_user.UserUpdatePasswordRequest) web_user.UserResponse
	FindByUsername(c *gin.Context, request web_user.UserRequest) web_user.UserResponse
	FindById(c *gin.Context, userId int) web_user.UserResponse
	VerifUser(c *gin.Context, userId int, token string)
	ResendSMS(c *gin.Context, username string)
}