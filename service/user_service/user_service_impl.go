package user_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_user"
	"final-project-bds-sanbercode-golang-batch-31/repository/user_repository"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/twilio/twilio-go"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserServiceImpl struct {
	UserRepository user_repository.UserRepository
	Validate *validator.Validate
}

func NewUserService(userRepository user_repository.UserRepository, validate *validator.Validate) UserService {
	return &UserServiceImpl{UserRepository: userRepository, Validate: validate}
}

func (u *UserServiceImpl) Login(c *gin.Context, request web_user.UserLoginRequest) web_user.UserLoginResponse {
	err := u.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	user := domain.User{
		Username:     request.Username,
		Password:     request.Password,
	}

	user, err = u.UserRepository.FindByUsername(c, db, user)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	if user.IsVerified == 0 {
		panic(exception.NewCustomValidationError("Silahkan lakukan verifikasi akun terlebih dahulu"))
	}

	err = helper.VerifyPassword(request.Password, user.Password)
	if err != nil {
		panic(exception.NewPasswordNotMatchError("Username / Password salah"))
	}

	token, err := helper.GenerateToken(user.IdUser)
	helper.PanicIfError(err)

	userResp := web_user.UserLoginResponse{
		IdUser:   user.IdUser,
		Username: user.Username,
		Phone:    user.Phone,
		Token: token,
		ExpireTime: helper.Getenv("TOKEN_HOUR_LIFESPAN", "1") + " hour",
	}

	return userResp
}


func (u *UserServiceImpl) Register(c *gin.Context, request web_user.UserRegisterRequest) web_user.UserResponse {
	err := u.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)


	_, err = u.UserRepository.FindByUsername(c, db, domain.User{Username: request.Username})
	if err == nil {
		panic(exception.NewCustomValidationError("Username sudah dipakai"))
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	helper.PanicIfError(err)

	user := domain.User{
		Username:     request.Username,
		Password:     string(hashedPassword),
		Phone:        request.Phone,
		VerifToken:   helper.RandomString(5),
	}

	user = u.UserRepository.Register(c, db, user)

	sms := web_user.UserSMSRequest{
		IdUser:   	user.IdUser,
		Phone:      user.Phone,
		VerifToken: user.VerifToken,
	}

	err = u.SendSMS(c, sms)
	if err != nil {
		u.UserRepository.DeleteUser(c, db, user.IdUser)
		panic(exception.NewCustomValidationError(err.Error()))
	}

	userResp := web_user.UserResponse{
		IdUser:   user.IdUser,
		Username: user.Username,
		Phone:    user.Phone,
	}

	return userResp
}

func (u *UserServiceImpl) SendSMS(c *gin.Context, request web_user.UserSMSRequest) error {
	err := u.Validate.Struct(request)
	helper.PanicIfError(err)

	accountSid := helper.Getenv("TWILLIO_SID", "")
	authToken := helper.Getenv("TWILLIO_TOKEN", "")
	client := twilio.NewRestClientWithParams(twilio.RestClientParams{
		Username: accountSid,
		Password: authToken,
	})

	params := &openapi.CreateMessageParams{}
	params.SetTo(request.Phone)
	params.SetFrom(helper.Getenv("TWILLIO_NUMBER", ""))
	params.SetBody(fmt.Sprintf("[TECH BLOG]\nverfication link: %s/api/v1/verification/%d/%s", helper.Getenv("BASE_URL", "http://localhost:8080"), request.IdUser, request.VerifToken))

	_, err = client.ApiV2010.CreateMessage(params)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (u *UserServiceImpl) UpdatePassword(c *gin.Context, request web_user.UserUpdatePasswordRequest) web_user.UserResponse {
	err := u.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	user := domain.User{
		Username:     request.Username,
		Password:     request.OldPassword,
		Phone:        request.Phone,
	}

	user, err = u.UserRepository.FindByUsername(c, db, user)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	err = helper.VerifyPassword(request.OldPassword, user.Password)
	if err != nil {
		panic(exception.NewPasswordNotMatchError("Username / Password salah"))
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(request.NewPassword), bcrypt.DefaultCost)
	helper.PanicIfError(err)

	user = domain.User{
		Username:     request.Username,
		Password:     string(hashedPassword),
		Phone:        request.Phone,
	}

	user = u.UserRepository.UpdatePassword(c, db, user)

	userResp := web_user.UserResponse{
		IdUser:   user.IdUser,
		Username: user.Username,
		Phone:    user.Phone,
	}

	return userResp
}

func (u *UserServiceImpl) FindByUsername(c *gin.Context, request web_user.UserRequest) web_user.UserResponse {
	err := u.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	user := domain.User{
		Username:     request.Username,
		Phone:        request.Phone,
	}

	user, err = u.UserRepository.FindByUsername(c, db, user)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	userResp := web_user.UserResponse{
		IdUser:   user.IdUser,
		Username: user.Username,
		Phone:    user.Phone,
	}

	return userResp
}

func (u *UserServiceImpl) FindById(c *gin.Context, userId int) web_user.UserResponse {
	db := c.MustGet("db").(*gorm.DB)

	user, err := u.UserRepository.FindById(c, db, userId)
	helper.PanicIfError(err)

	userResp := web_user.UserResponse{
		IdUser:   user.IdUser,
		Username: user.Username,
		Phone:    user.Phone,
	}

	return userResp
}

func (u *UserServiceImpl) VerifUser(c *gin.Context, userId int, token string) {
	db := c.MustGet("db").(*gorm.DB)

	_, err := u.UserRepository.FindById(c, db, userId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	err = u.UserRepository.VerifUser(c, db, userId, token)
	helper.PanicIfError(err)
}


func (u *UserServiceImpl) ResendSMS(c *gin.Context, username string) {
	db := c.MustGet("db").(*gorm.DB)

	user := domain.User{
		Username:     username,
		Phone:        "",
	}

	user, err := u.UserRepository.FindByUsername(c, db, user)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	if user.IsVerified == 1 {
		panic(exception.NewCustomValidationError("Akun sudah terverifikasi"))
	}

	smsReq := web_user.UserSMSRequest{
		IdUser:     user.IdUser,
		Phone:      user.Phone,
		VerifToken: user.VerifToken,
	}

	err = u.SendSMS(c, smsReq)
	helper.PanicIfError(err)
}
