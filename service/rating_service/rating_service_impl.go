package rating_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_rating"
	"final-project-bds-sanbercode-golang-batch-31/repository/rating_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type RatingServiceImpl struct {
	RatingRepository rating_repository.RatingRepository
	ThreadRepository thread_repository.ThreadRepository
	Validate *validator.Validate
}

func NewRatingService(
	ratingRepository rating_repository.RatingRepository,
	threadRepository thread_repository.ThreadRepository,
	validate *validator.Validate,
) RatingService {
	return &RatingServiceImpl{
		RatingRepository: ratingRepository,
		ThreadRepository: threadRepository,
		Validate: validate,
	}
}

func (r *RatingServiceImpl) Rate(c *gin.Context, request web_rating.RatingCreateRequest)  {
	err := r.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	_, err = r.ThreadRepository.FindById(c, db, request.IdThreadFk)
	if err != nil {
		panic(exception.NewNotFoundError("Thread tidak ditemukan"))
	}

	_, err = r.RatingRepository.FindById(c, db, request.IdUserFk, request.IdThreadFk)
	if err == nil {
		panic(exception.NewCustomValidationError("Thread sudah diberi rating"))
	}

	rating := domain.Rating{
		Rating:     request.Rating,
		IdUserFk:   request.IdUserFk,
		IdThreadFk: request.IdThreadFk,
	}

	rating = r.RatingRepository.Save(c, db, rating)
}

func (r *RatingServiceImpl) ChangeRate(c *gin.Context, request web_rating.RatingCreateRequest) {
	err := r.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	rate, err := r.RatingRepository.FindById(c, db, request.IdUserFk, request.IdThreadFk)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	rating := domain.Rating{
		IdRating:   rate.IdRating,
		Rating:     request.Rating,
	}

	rating = r.RatingRepository.Update(c, db, rating)
}

func (r *RatingServiceImpl) CalculateOverall(c *gin.Context, threadId int) float64 {
	db := c.MustGet("db").(*gorm.DB)

	return r.RatingRepository.CalculateOverall(c, db, threadId)
}


