package rating_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_rating"
	"github.com/gin-gonic/gin"
)

type RatingService interface {
	Rate(c *gin.Context, request web_rating.RatingCreateRequest)
	ChangeRate(c *gin.Context, request web_rating.RatingCreateRequest)
	CalculateOverall(c *gin.Context, threadId int) float64
}
