package image_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"github.com/gin-gonic/gin"
)

type ImageService interface {
	Add(c *gin.Context, request web_image.ImageRequest)
	Delete(c *gin.Context, userId, imageId int)
}
