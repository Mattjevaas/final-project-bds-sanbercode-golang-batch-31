package image_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"final-project-bds-sanbercode-golang-batch-31/repository/image_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type ImageServiceImpl struct {
	ImageRepository image_repository.ImageRepository
	ThreadRepository thread_repository.ThreadRepository
	Validate *validator.Validate
}

func NewImageService(
	imageRepository image_repository.ImageRepository,
	threadRepository thread_repository.ThreadRepository,
	validate *validator.Validate,
) ImageService {
	return &ImageServiceImpl{
		ImageRepository: imageRepository,
		ThreadRepository: threadRepository,
		Validate: validate,
	}
}

func (i *ImageServiceImpl) Add(c *gin.Context, request web_image.ImageRequest) {
	err := i.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	thread, err := i.ThreadRepository.FindById(c ,db, request.IdThreadFk)
	if err != nil {
		panic(exception.NewNotFoundError("Thread tidak ditemukan"))
	}

	if thread.IdUserFk != request.IdUserFk {
		panic(exception.NewCustomValidationError("Thread ini bukan milik anda"))
	}

	image := domain.Image{
		ImageUrl:   request.ImageUrl,
		IdThreadFk: request.IdThreadFk,
	}

	i.ImageRepository.Add(c, db, image)
}

func (i *ImageServiceImpl) Delete(c *gin.Context, userId, imageId int) {
	db := c.MustGet("db").(*gorm.DB)

	image, err := i.ImageRepository.FindById(c, db, imageId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	thread, err := i.ThreadRepository.FindById(c ,db, image.IdThreadFk)
	if err != nil {
		panic(exception.NewNotFoundError("Thread tidak ditemukan"))
	}

	if thread.IdUserFk != userId {
		panic(exception.NewCustomValidationError("Thread ini bukan milik anda"))
	}

	i.ImageRepository.Delete(c, db, imageId)
}
