package comment_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/helper"
	"final-project-bds-sanbercode-golang-batch-31/model/domain"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"final-project-bds-sanbercode-golang-batch-31/repository/comment_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type CommentServiceImpl struct {
	CommentRepository comment_repository.CommentRepository
	ThreadRepository thread_repository.ThreadRepository
	Validate *validator.Validate
}

func NewCommentService(
	commentRepository comment_repository.CommentRepository,
	threadRepository thread_repository.ThreadRepository,
	validate *validator.Validate,
) CommentService {
	return &CommentServiceImpl{
		CommentRepository: commentRepository,
		ThreadRepository: threadRepository,
		Validate: validate,
	}
}

func (co *CommentServiceImpl) CreateComment(c *gin.Context, request web_comment.CommentCreateRequest) {
	err := co.Validate.Struct(request)
	helper.PanicIfError(err)

	db := c.MustGet("db").(*gorm.DB)

	_, err = co.ThreadRepository.FindById(c, db, request.IdThreadFk)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	comment := domain.Comment{
		Content:     request.Content,
		IdThreadFk:  request.IdThreadFk,
		IdUserFk:    request.IdUserFk,
	}

	co.CommentRepository.Add(c, db, comment)
}

func (co *CommentServiceImpl) Delete(c *gin.Context, userId, commentId int) {
	db := c.MustGet("db").(*gorm.DB)

	_, err := co.CommentRepository.FindById(c, db, userId, commentId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	co.CommentRepository.Delete(c, db, userId, commentId)
}


