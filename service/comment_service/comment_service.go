package comment_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"github.com/gin-gonic/gin"
)

type CommentService interface {
	CreateComment(c *gin.Context, request web_comment.CommentCreateRequest)
	Delete(c *gin.Context, userId, commentId int)
}
