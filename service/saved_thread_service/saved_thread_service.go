package saved_thread_service

import (
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_saved_thread"
	"github.com/gin-gonic/gin"
)

type SavedThreadService interface {
	SaveThread(c *gin.Context, userId, threadId int)
	DeleteSavedThread(c *gin.Context, userId, threadId int)
	FindAllSavedThread(c *gin.Context, userId int) []web_saved_thread.SavedThreadResponse
}
