package saved_thread_service

import (
	"final-project-bds-sanbercode-golang-batch-31/exception"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_comment"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_image"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_saved_thread"
	"final-project-bds-sanbercode-golang-batch-31/model/web/web_thread"
	"final-project-bds-sanbercode-golang-batch-31/repository/comment_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/image_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/like_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/rating_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/saved_thread_repository"
	"final-project-bds-sanbercode-golang-batch-31/repository/thread_repository"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type SavedThreadServiceImpl struct {
	SavedThreadRepository saved_thread_repository.SavedThreadRepository
	ThreadRepository thread_repository.ThreadRepository
	RatingRepository rating_repository.RatingRepository
	CommentRepository comment_repository.CommentRepository
	LikeRepository like_repository.LikeRepository
	ImageRepository image_repository.ImageRepository
}

func NewSavedThreadService(
	savedThreadRepository saved_thread_repository.SavedThreadRepository,
	threadRepository thread_repository.ThreadRepository,
	ratingRepository rating_repository.RatingRepository,
	imageRepository image_repository.ImageRepository,
	commentRepository comment_repository.CommentRepository,
	likeRepository like_repository.LikeRepository,
) SavedThreadService {
	return &SavedThreadServiceImpl{
		SavedThreadRepository: savedThreadRepository,
		ThreadRepository:      threadRepository,
		RatingRepository:      ratingRepository,
		CommentRepository:     commentRepository,
		LikeRepository:        likeRepository,
		ImageRepository:       imageRepository,
	}
}


func (s *SavedThreadServiceImpl) SaveThread(c *gin.Context, userId, threadId int) {

	db := c.MustGet("db").(*gorm.DB)

	thread, err := s.ThreadRepository.FindById(c, db, threadId)
	if thread.IdUserFk == userId{
		panic(exception.NewCustomValidationError("Tidak bisa save thread pribadi"))
	}

	_ , err = s.SavedThreadRepository.FindById(c, db, userId, threadId)
	if err == nil {
		panic(exception.NewCustomValidationError("Thread sudah disave sebelumnya"))
	}

	s.SavedThreadRepository.SaveThread(c, db, userId, threadId)
}

func (s *SavedThreadServiceImpl) DeleteSavedThread(c *gin.Context, userId, threadId int) {
	db := c.MustGet("db").(*gorm.DB)

	_, err := s.SavedThreadRepository.FindById(c, db, userId, threadId)
	if err != nil{
		panic(exception.NewNotFoundError(err.Error()))
	}

	s.SavedThreadRepository.DeleteSavedThread(c, db, userId, threadId)
}

func (s *SavedThreadServiceImpl) FindAllSavedThread(c *gin.Context, userId int) []web_saved_thread.SavedThreadResponse {
	db := c.MustGet("db").(*gorm.DB)

	saveds := s.SavedThreadRepository.FindAllSavedThread(c, db, userId)

	savedsResp := []web_saved_thread.SavedThreadResponse{}
	for _, saved := range saveds{

		thread, err := s.ThreadRepository.FindById(c, db, saved.IdThreadFk)

		if err == nil {

			imagesResponse := make(chan []web_image.ImageResponse)
			images := s.ImageRepository.FindAll(c, db, thread.IdThread)

			go func(c chan []web_image.ImageResponse){
				imagesRes := []web_image.ImageResponse{}
				for _, image := range images {
					temp := web_image.ImageResponse{
						IdImage:   image.IdImage,
						ImageUrl:  image.ImageUrl,
					}
					imagesRes = append( imagesRes, temp)
				}

				c <- imagesRes
			}(imagesResponse)

			commentResponse := make(chan []web_comment.CommentResponse)
			comments := s.CommentRepository.FindAll(c, db, thread.IdThread)

			go func(ch chan []web_comment.CommentResponse){
				commentRes := []web_comment.CommentResponse{}
				for _, comment := range comments {

					like := s.LikeRepository.Count(c, db, comment.IdComment)

					temp := web_comment.CommentResponse{
						IdComment: comment.IdComment,
						IdUserFk: comment.IdUserFk,
						Content:   comment.Content,
						Likes:     like,
						CreatedAt: comment.CreatedAt,
						UpdatedAt: comment.UpdatedAt,
					}
					commentRes = append(commentRes, temp)
				}

				ch <- commentRes
			}(commentResponse)

			ratings := s.RatingRepository.CalculateOverall(c, db, thread.IdThread)

			threadResp := web_thread.ThreadResponse{
				IdThread:   thread.IdThread,
				IdUserFk:   thread.IdUserFk,
				Title:      thread.Title,
				Content:    thread.Content,
				Rating:     ratings,
				ImagesUrl:  <- imagesResponse,
				Comments:   <- commentResponse,
				Created_at: thread.CreatedAt,
				Updated_at: thread.UpdatedAt,
			}

			temp := web_saved_thread.SavedThreadResponse{
				IdSavedThread: saved.IdSavedThread,
				Thread:threadResp,
			}

			savedsResp = append(savedsResp, temp)
		}
	}


	return savedsResp
}