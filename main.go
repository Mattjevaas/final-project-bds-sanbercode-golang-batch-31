package main

import (
	"final-project-bds-sanbercode-golang-batch-31/docs"
	"final-project-bds-sanbercode-golang-batch-31/injector"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/
func main(){
	//err := godotenv.Load()
	//helper.PanicIfError(err)

	docs.SwaggerInfo.Title = "Swagger TECH BLOG API by Johanes Wiku Sakti"
	docs.SwaggerInfo.Description = "This is a Tech Blog API (For Sanbercode Golang Final Project)."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "techblog-app-sanbercode-golang.herokuapp.com/api/v1"
	//docs.SwaggerInfo.Host = "localhost:8080/api/v1"
	docs.SwaggerInfo.Schemes = []string{"https"}
	router := injector.InitializeServer()
	router.Run()
}